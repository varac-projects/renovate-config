# Renovate config

## Todo

- `truecharts` helm charts are not beeing renovated, see i.e.
  [this pipeline](https://0xacab.org/varac-projects/renovate-config/-/jobs/456005#L4111):

  ```json
  "packageFile": "media.varac.net/media/bazarr-helmrelease.yaml",
    "deps": [
      {
        "depName": "bazarr",
        "currentValue": "15.0.1",
        "datasource": "helm",
        "skipReason": "unknown-registry",
        "updates": [],
        "packageName": "bazarr"
      }
    ]
  },
  ```

  - See `./flux.json` and the `registryAliases` config keyword

## Docs

- [Support renovating apk add commands in Dockerfiles](https://github.com/renovatebot/renovate/issues/5422#issuecomment-645947041)
